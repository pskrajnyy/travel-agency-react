import Icon from '../components/common/Icon/Icon';
import React from 'react';

export const phone = () => {
  let phone = null;
  const currentTime = new Date();
  const eightTime = new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), currentTime.getUTCDate(), 8, 0, 0, 0));
  const twelveTime = new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), currentTime.getUTCDate(), 12, 0, 0, 0));
  const sixteenTime = new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), currentTime.getUTCDate(), 16, 0, 0, 0));
  const twentyTwoTime = new Date(Date.UTC(currentTime.getUTCFullYear(), currentTime.getUTCMonth(), currentTime.getUTCDate(), 22, 0, 0, 0));

  if (currentTime.getTime() >= eightTime.getTime() && currentTime.getTime() < twelveTime.getTime()) {
    phone = <span><Icon name='phone' /> Amanda, 678.243.8455</span>;
  } else if (currentTime.getTime() >= twelveTime.getTime() && currentTime.getTime() < sixteenTime.getTime()) {
    phone = <span><Icon name='phone' /> Tobias, 278.443.6443</span>;
  } else if (currentTime.getTime() >= sixteenTime.getTime() && currentTime.getTime() < twentyTwoTime.getTime()) {
    phone = <span><Icon name='phone' /> Helena, 167.280.3970</span>;
  } else {
    phone = <span>The office opens at 8:00 UTC</span>;
  }

  return phone;
};

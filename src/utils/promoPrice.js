export const promoPrice = (standardPrice, percent) => {
  const priceParsed = standardPrice.replace(/^\$\s*/, '').replace(/,/g, '');
  return priceParsed * ((100 - percent)/100);
};

import React from 'react';
import styles from './HappyHourAd.scss';
import { formatTime } from '../../../utils/formatTime';
import {promoTimer} from '../../../utils/promoTimer';

class HappyHourAd extends React.Component {

  constructor(){
    super();
    setInterval(() => this.forceUpdate(), 1000);
  }

  render() {
    let dynamicPromoInput;
    if (promoTimer() === true) {
      dynamicPromoInput = 'It\'s your time! Take advantage of Happy Hour! All offers 20% off!';
    }
    else {
      dynamicPromoInput = formatTime(promoTimer()[1]);
    }
    return(
      <div className={styles.component}>
        <h3 className={styles.title}>Happy Hour</h3>
        <div className={styles.promoDescription}>
          {dynamicPromoInput}
        </div>
      </div>
    );
  }
}

export default HappyHourAd;

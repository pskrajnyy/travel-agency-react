import React from 'react';
import {shallow} from 'enzyme';
import TripSummary from './TripSummary';

const number = 7;

describe('Component TripSummary', () => {
  it('should generate proper URL address', () => {
    const expectedURL = 'abc';
    const component = shallow(<TripSummary id={expectedURL} intro="" image="" name="" cost="" days={number} tags={[]}/>);
    expect(component.find('.link').prop('to')).toEqual('/trip/' + expectedURL);
  });

  it('should render image (with proper src && alt)', () => {
    const expectedSrc = 'image';
    const expectedAlt = 'image alt';
    const component = shallow(<TripSummary id={number} intro="" image={expectedSrc} name={expectedAlt} cost="" days={number} tags={[]}/>);
    expect(component.find('img').prop('src')).toEqual(expectedSrc);
    expect(component.find('img').prop('alt')).toEqual(expectedAlt);
  });

  it('should render name, cost and days props', () => {
    const expectedName = 'Lorem';
    const expectedCost = '$99';
    const expectedDays = 7;
    const component = shallow(<TripSummary id={number} intro="" image="" name={expectedName} cost={expectedCost} days={expectedDays} tags={[]}/>);
    const renderedName = component.find('.title').text();
    const renderedDays = component.find('.details span:first-child').text();
    const renderedCost = component.find('.details span:last-child').text();
    expect(renderedName).toEqual(expectedName);
    expect(renderedDays).toEqual(`${expectedDays} days`);
    expect(renderedCost).toEqual(`from ${expectedCost}`);
  });

  it('should throw error without required props', () => {
    expect(() => shallow(<TripSummary />)).toThrow();
  });

  it('should render tags without crashing', () => {
    const tags = ['first', 'second', 'third'];
    const component = shallow(<TripSummary id="" intro="" image="" name="" cost="" days={number} tags={tags}/>);
    expect(component.find('.tag').at(0)).toEqual[tags[0]];
    expect(component.find('.tag').at(1)).toEqual[tags[1]];
    expect(component.find('.tag').at(2)).toEqual[tags[2]];
  });

  it('should crash if tags array is missing or hollow', () => {
    const component = shallow(<TripSummary id="" intro="" image="" name="" cost="" days={number} tags={[]}/>);
    expect(component.find('.tags')).toBeTruthy();
  });
});

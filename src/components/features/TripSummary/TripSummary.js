import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import styles from './TripSummary.scss';
import {Col} from 'react-flexbox-grid';
import {promoTimer} from '../../../utils/promoTimer';
import {promoPrice} from '../../../utils/promoPrice';
import {formatPrice} from '../../../utils/formatPrice';

class TripSummary extends React.Component {

  render() {
    const {id, image, name, cost, days, tags} = this.props;
    let renderTags = null;
    let renderPromoPrice = null;

    if(tags && tags.length > 0) {
      renderTags = <div className={styles.tags}>
        {tags.map(tag => (
          <span className={styles.tag} key={tag.toString()}>{tag}</span>
        ))}
      </div>;
    }

    if(promoTimer() === true) {
      renderPromoPrice = <div>
        <span>Promo price: {formatPrice(promoPrice(cost, 20))}</span>
      </div>;
    }

    return (
      <Col xs={12} sm={6} lg={4} className={styles.column}>
        <Link to={`/trip/${id}`} className={styles.link}>
          <article className={styles.component}>
            <img src={image} alt={name} />
            <h3 className={styles.title}>{name}</h3>
            <div className={styles.details}>
              <span>{days} days</span>
              {renderPromoPrice}
              <span>from {cost}</span>
            </div>
            {renderTags}
          </article>
        </Link>
      </Col>
    );
  }
}

TripSummary.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  intro: PropTypes.string.isRequired,
  cost: PropTypes.string.isRequired,
  days: PropTypes.number.isRequired,
  tags: PropTypes.array.isRequired,
};

export default TripSummary;

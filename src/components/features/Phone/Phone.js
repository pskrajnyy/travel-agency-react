import React from 'react';
import {phone} from '../../../utils/phone';

class Phone extends React.Component {
  render() {
    return(
      <div>
        <p>{phone()}</p>
      </div>
    );
  }
}

export default Phone;

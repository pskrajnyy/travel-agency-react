# Project description
This is application for Travel Agency. Application implements in React.
# Project objective
The main purpose of this project was to play a little bit with React.
# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/) cloud . You can test it by yourself on this [website](https://travel-react.herokuapp.com).
